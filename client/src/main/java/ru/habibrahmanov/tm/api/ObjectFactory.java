
package ru.habibrahmanov.tm.api;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.habibrahmanov.tm.api package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _IncorrectValueException_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "IncorrectValueException");
    private final static QName _ListIsEmptyExeption_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "ListIsEmptyExeption");
    private final static QName _NoSuchAlgorithmException_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "NoSuchAlgorithmException");
    private final static QName _SessionIsNotValidException_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "SessionIsNotValidException");
    private final static QName _UnsupportedEncodingException_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "UnsupportedEncodingException");
    private final static QName _EditProfile_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "editProfile");
    private final static QName _EditProfileResponse_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "editProfileResponse");
    private final static QName _FindAll_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "findAll");
    private final static QName _FindAllResponse_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "findAllResponse");
    private final static QName _FindByLogin_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "findByLogin");
    private final static QName _FindByLoginResponse_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "findByLoginResponse");
    private final static QName _GetCurrentUser_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "getCurrentUser");
    private final static QName _GetCurrentUserResponse_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "getCurrentUserResponse");
    private final static QName _IsAuth_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "isAuth");
    private final static QName _IsAuthResponse_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "isAuthResponse");
    private final static QName _Login_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "login");
    private final static QName _LoginResponse_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "loginResponse");
    private final static QName _Logout_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "logout");
    private final static QName _LogoutResponse_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "logoutResponse");
    private final static QName _RegistryAdmin_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "registryAdmin");
    private final static QName _RegistryAdminResponse_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "registryAdminResponse");
    private final static QName _RegistryUser_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "registryUser");
    private final static QName _RegistryUserResponse_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "registryUserResponse");
    private final static QName _UpdatePassword_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "updatePassword");
    private final static QName _UpdatePasswordResponse_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "updatePasswordResponse");
    private final static QName _ViewProfile_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "viewProfile");
    private final static QName _ViewProfileResponse_QNAME = new QName("http://api.tm.habibrahmanov.ru/", "viewProfileResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.habibrahmanov.tm.api
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IncorrectValueException }
     * 
     */
    public IncorrectValueException createIncorrectValueException() {
        return new IncorrectValueException();
    }

    /**
     * Create an instance of {@link ListIsEmptyExeption }
     * 
     */
    public ListIsEmptyExeption createListIsEmptyExeption() {
        return new ListIsEmptyExeption();
    }

    /**
     * Create an instance of {@link NoSuchAlgorithmException }
     * 
     */
    public NoSuchAlgorithmException createNoSuchAlgorithmException() {
        return new NoSuchAlgorithmException();
    }

    /**
     * Create an instance of {@link SessionIsNotValidException }
     * 
     */
    public SessionIsNotValidException createSessionIsNotValidException() {
        return new SessionIsNotValidException();
    }

    /**
     * Create an instance of {@link UnsupportedEncodingException }
     * 
     */
    public UnsupportedEncodingException createUnsupportedEncodingException() {
        return new UnsupportedEncodingException();
    }

    /**
     * Create an instance of {@link EditProfile }
     * 
     */
    public EditProfile createEditProfile() {
        return new EditProfile();
    }

    /**
     * Create an instance of {@link EditProfileResponse }
     * 
     */
    public EditProfileResponse createEditProfileResponse() {
        return new EditProfileResponse();
    }

    /**
     * Create an instance of {@link FindAll }
     * 
     */
    public FindAll createFindAll() {
        return new FindAll();
    }

    /**
     * Create an instance of {@link FindAllResponse }
     * 
     */
    public FindAllResponse createFindAllResponse() {
        return new FindAllResponse();
    }

    /**
     * Create an instance of {@link FindByLogin }
     * 
     */
    public FindByLogin createFindByLogin() {
        return new FindByLogin();
    }

    /**
     * Create an instance of {@link FindByLoginResponse }
     * 
     */
    public FindByLoginResponse createFindByLoginResponse() {
        return new FindByLoginResponse();
    }

    /**
     * Create an instance of {@link GetCurrentUser }
     * 
     */
    public GetCurrentUser createGetCurrentUser() {
        return new GetCurrentUser();
    }

    /**
     * Create an instance of {@link GetCurrentUserResponse }
     * 
     */
    public GetCurrentUserResponse createGetCurrentUserResponse() {
        return new GetCurrentUserResponse();
    }

    /**
     * Create an instance of {@link IsAuth }
     * 
     */
    public IsAuth createIsAuth() {
        return new IsAuth();
    }

    /**
     * Create an instance of {@link IsAuthResponse }
     * 
     */
    public IsAuthResponse createIsAuthResponse() {
        return new IsAuthResponse();
    }

    /**
     * Create an instance of {@link Login }
     * 
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link Logout }
     * 
     */
    public Logout createLogout() {
        return new Logout();
    }

    /**
     * Create an instance of {@link LogoutResponse }
     * 
     */
    public LogoutResponse createLogoutResponse() {
        return new LogoutResponse();
    }

    /**
     * Create an instance of {@link RegistryAdmin }
     * 
     */
    public RegistryAdmin createRegistryAdmin() {
        return new RegistryAdmin();
    }

    /**
     * Create an instance of {@link RegistryAdminResponse }
     * 
     */
    public RegistryAdminResponse createRegistryAdminResponse() {
        return new RegistryAdminResponse();
    }

    /**
     * Create an instance of {@link RegistryUser }
     * 
     */
    public RegistryUser createRegistryUser() {
        return new RegistryUser();
    }

    /**
     * Create an instance of {@link RegistryUserResponse }
     * 
     */
    public RegistryUserResponse createRegistryUserResponse() {
        return new RegistryUserResponse();
    }

    /**
     * Create an instance of {@link UpdatePassword }
     * 
     */
    public UpdatePassword createUpdatePassword() {
        return new UpdatePassword();
    }

    /**
     * Create an instance of {@link UpdatePasswordResponse }
     * 
     */
    public UpdatePasswordResponse createUpdatePasswordResponse() {
        return new UpdatePasswordResponse();
    }

    /**
     * Create an instance of {@link ViewProfile }
     * 
     */
    public ViewProfile createViewProfile() {
        return new ViewProfile();
    }

    /**
     * Create an instance of {@link ViewProfileResponse }
     * 
     */
    public ViewProfileResponse createViewProfileResponse() {
        return new ViewProfileResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IncorrectValueException }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IncorrectValueException }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "IncorrectValueException")
    public JAXBElement<IncorrectValueException> createIncorrectValueException(IncorrectValueException value) {
        return new JAXBElement<IncorrectValueException>(_IncorrectValueException_QNAME, IncorrectValueException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListIsEmptyExeption }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ListIsEmptyExeption }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "ListIsEmptyExeption")
    public JAXBElement<ListIsEmptyExeption> createListIsEmptyExeption(ListIsEmptyExeption value) {
        return new JAXBElement<ListIsEmptyExeption>(_ListIsEmptyExeption_QNAME, ListIsEmptyExeption.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoSuchAlgorithmException }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link NoSuchAlgorithmException }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "NoSuchAlgorithmException")
    public JAXBElement<NoSuchAlgorithmException> createNoSuchAlgorithmException(NoSuchAlgorithmException value) {
        return new JAXBElement<NoSuchAlgorithmException>(_NoSuchAlgorithmException_QNAME, NoSuchAlgorithmException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SessionIsNotValidException }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SessionIsNotValidException }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "SessionIsNotValidException")
    public JAXBElement<SessionIsNotValidException> createSessionIsNotValidException(SessionIsNotValidException value) {
        return new JAXBElement<SessionIsNotValidException>(_SessionIsNotValidException_QNAME, SessionIsNotValidException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnsupportedEncodingException }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UnsupportedEncodingException }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "UnsupportedEncodingException")
    public JAXBElement<UnsupportedEncodingException> createUnsupportedEncodingException(UnsupportedEncodingException value) {
        return new JAXBElement<UnsupportedEncodingException>(_UnsupportedEncodingException_QNAME, UnsupportedEncodingException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditProfile }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EditProfile }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "editProfile")
    public JAXBElement<EditProfile> createEditProfile(EditProfile value) {
        return new JAXBElement<EditProfile>(_EditProfile_QNAME, EditProfile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditProfileResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EditProfileResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "editProfileResponse")
    public JAXBElement<EditProfileResponse> createEditProfileResponse(EditProfileResponse value) {
        return new JAXBElement<EditProfileResponse>(_EditProfileResponse_QNAME, EditProfileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAll }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindAll }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "findAll")
    public JAXBElement<FindAll> createFindAll(FindAll value) {
        return new JAXBElement<FindAll>(_FindAll_QNAME, FindAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindAllResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "findAllResponse")
    public JAXBElement<FindAllResponse> createFindAllResponse(FindAllResponse value) {
        return new JAXBElement<FindAllResponse>(_FindAllResponse_QNAME, FindAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByLogin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindByLogin }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "findByLogin")
    public JAXBElement<FindByLogin> createFindByLogin(FindByLogin value) {
        return new JAXBElement<FindByLogin>(_FindByLogin_QNAME, FindByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByLoginResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindByLoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "findByLoginResponse")
    public JAXBElement<FindByLoginResponse> createFindByLoginResponse(FindByLoginResponse value) {
        return new JAXBElement<FindByLoginResponse>(_FindByLoginResponse_QNAME, FindByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrentUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetCurrentUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "getCurrentUser")
    public JAXBElement<GetCurrentUser> createGetCurrentUser(GetCurrentUser value) {
        return new JAXBElement<GetCurrentUser>(_GetCurrentUser_QNAME, GetCurrentUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrentUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetCurrentUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "getCurrentUserResponse")
    public JAXBElement<GetCurrentUserResponse> createGetCurrentUserResponse(GetCurrentUserResponse value) {
        return new JAXBElement<GetCurrentUserResponse>(_GetCurrentUserResponse_QNAME, GetCurrentUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsAuth }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsAuth }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "isAuth")
    public JAXBElement<IsAuth> createIsAuth(IsAuth value) {
        return new JAXBElement<IsAuth>(_IsAuth_QNAME, IsAuth.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsAuthResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsAuthResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "isAuthResponse")
    public JAXBElement<IsAuthResponse> createIsAuthResponse(IsAuthResponse value) {
        return new JAXBElement<IsAuthResponse>(_IsAuthResponse_QNAME, IsAuthResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Login }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Login }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "login")
    public JAXBElement<Login> createLogin(Login value) {
        return new JAXBElement<Login>(_Login_QNAME, Login.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "loginResponse")
    public JAXBElement<LoginResponse> createLoginResponse(LoginResponse value) {
        return new JAXBElement<LoginResponse>(_LoginResponse_QNAME, LoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Logout }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Logout }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "logout")
    public JAXBElement<Logout> createLogout(Logout value) {
        return new JAXBElement<Logout>(_Logout_QNAME, Logout.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogoutResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LogoutResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "logoutResponse")
    public JAXBElement<LogoutResponse> createLogoutResponse(LogoutResponse value) {
        return new JAXBElement<LogoutResponse>(_LogoutResponse_QNAME, LogoutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistryAdmin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RegistryAdmin }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "registryAdmin")
    public JAXBElement<RegistryAdmin> createRegistryAdmin(RegistryAdmin value) {
        return new JAXBElement<RegistryAdmin>(_RegistryAdmin_QNAME, RegistryAdmin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistryAdminResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RegistryAdminResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "registryAdminResponse")
    public JAXBElement<RegistryAdminResponse> createRegistryAdminResponse(RegistryAdminResponse value) {
        return new JAXBElement<RegistryAdminResponse>(_RegistryAdminResponse_QNAME, RegistryAdminResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistryUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RegistryUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "registryUser")
    public JAXBElement<RegistryUser> createRegistryUser(RegistryUser value) {
        return new JAXBElement<RegistryUser>(_RegistryUser_QNAME, RegistryUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistryUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RegistryUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "registryUserResponse")
    public JAXBElement<RegistryUserResponse> createRegistryUserResponse(RegistryUserResponse value) {
        return new JAXBElement<RegistryUserResponse>(_RegistryUserResponse_QNAME, RegistryUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdatePassword }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdatePassword }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "updatePassword")
    public JAXBElement<UpdatePassword> createUpdatePassword(UpdatePassword value) {
        return new JAXBElement<UpdatePassword>(_UpdatePassword_QNAME, UpdatePassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdatePasswordResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdatePasswordResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "updatePasswordResponse")
    public JAXBElement<UpdatePasswordResponse> createUpdatePasswordResponse(UpdatePasswordResponse value) {
        return new JAXBElement<UpdatePasswordResponse>(_UpdatePasswordResponse_QNAME, UpdatePasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewProfile }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ViewProfile }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "viewProfile")
    public JAXBElement<ViewProfile> createViewProfile(ViewProfile value) {
        return new JAXBElement<ViewProfile>(_ViewProfile_QNAME, ViewProfile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewProfileResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ViewProfileResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.habibrahmanov.ru/", name = "viewProfileResponse")
    public JAXBElement<ViewProfileResponse> createViewProfileResponse(ViewProfileResponse value) {
        return new JAXBElement<ViewProfileResponse>(_ViewProfileResponse_QNAME, ViewProfileResponse.class, null, value);
    }

}
