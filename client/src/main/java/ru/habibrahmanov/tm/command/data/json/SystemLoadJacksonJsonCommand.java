package ru.habibrahmanov.tm.command.data.json;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class SystemLoadJacksonJsonCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "load-jackson-json";
    }

    @Override
    public String getDescription() {
        return "loading the subject area using externalization and jackson technology in json transport format";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getDomainEndpoint().loadJacksonJson(serviceLocator.getCurrentSession());
        System.out.println("OK");
    }
}
