package ru.habibrahmanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.api.Task;

import java.util.List;

public class TaskFindAllSortedByStatusCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task-sort-status";
    }

    @Override
    public String getDescription() {
        return "task sort by status";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND ALL TASKS SORTED BY DATE END]");
        @NotNull final List<Task> taskList = serviceLocator.getTaskEndpoint().findAllSortedByStatusTask(serviceLocator.getCurrentSession());
        for (Task task : taskList) {
            System.out.println(task);
            System.out.println();
        }
    }
}
