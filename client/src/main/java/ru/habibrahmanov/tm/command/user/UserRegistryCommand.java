package ru.habibrahmanov.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.Role;
import ru.habibrahmanov.tm.api.User;
import ru.habibrahmanov.tm.command.AbstractCommand;

public final class UserRegistryCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-registry";
    }

    @Override
    public String getDescription() {
        return "new user registration";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CREATE NEW USER]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = serviceLocator.getScanner().nextLine();
        User user = new User();
        user.setLogin(login);
        user.setRole(Role.USER);
        user.setPassword(password);
        serviceLocator.getUserEndpoint().registryUser(user);
        System.out.println("CREATE NEW USER SUCCESSFULLY");
    }
}
