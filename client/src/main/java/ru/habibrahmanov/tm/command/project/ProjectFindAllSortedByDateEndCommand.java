package ru.habibrahmanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.api.Project;
import java.util.List;

public class ProjectFindAllSortedByDateEndCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-sort-data-end";
    }

    @Override
    public String getDescription() {
        return "project find all sorted by data end command";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND ALL PROJECTS SORTED BY DATE END]");
        @NotNull final List<Project> projectList = serviceLocator.getProjectEndpoint().findAllSortedByDateEndProject(serviceLocator.getCurrentSession());
        for (Project project : projectList) {
            System.out.println(project);
            System.out.println();
        }
    }
}
