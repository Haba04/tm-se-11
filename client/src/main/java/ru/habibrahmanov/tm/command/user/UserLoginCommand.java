package ru.habibrahmanov.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.command.AbstractCommand;

public final class UserLoginCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "login";
    }

    @Override
    public String getDescription() {
        return "user authorization in the data";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[AUTHORIZATION]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = serviceLocator.getScanner().nextLine();
//        if (serviceLocator.getUserEndpoint().findByLogin(login).getPassword().equals(password))
        serviceLocator.setCurrentSession(serviceLocator.getSessionEndpoint().open(login, password));
        System.out.println("YOU ARE LOGGED IN");
    }
}
