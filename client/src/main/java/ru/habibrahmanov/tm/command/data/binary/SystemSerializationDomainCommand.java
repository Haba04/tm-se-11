package ru.habibrahmanov.tm.command.data.binary;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class SystemSerializationDomainCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "serialization-domain";
    }

    @Override
    public String getDescription() {
        return "saving a domain using serialization";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getDomainEndpoint().serializationDomain(serviceLocator.getCurrentSession());
        System.out.println("OK");
    }
}
