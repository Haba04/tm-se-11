package ru.habibrahmanov.tm.command.user;

import ru.habibrahmanov.tm.command.AbstractCommand;

public final class UserLogoutCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "logout";
    }

    @Override
    public String getDescription() {
        return "you are logged out";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getUserEndpoint().logout(serviceLocator.getCurrentSession());
        System.out.println("YOU ARE LOGGED OUT");
    }
}
