package ru.habibrahmanov.tm.command.task;

import ru.habibrahmanov.tm.api.Task;
import ru.habibrahmanov.tm.command.AbstractCommand;

public final class TaskFindAllCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-find-all";
    }

    @Override
    public String getDescription() {
        return "find all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW ALL TASKS]");
        for (Task task : serviceLocator.getTaskEndpoint().findAllTask(serviceLocator.getCurrentSession())) {
            System.out.println("TASK ID: " + task.getId());
            System.out.println("NAME: " + task.getName() + ", DESCRIPTION: " + task.getDescription());
            System.out.println("DATE BEGIN: " + task.getDateBegin() + ", DATE END: " + task.getDateEnd());
            System.out.println();
        }

    }
}
