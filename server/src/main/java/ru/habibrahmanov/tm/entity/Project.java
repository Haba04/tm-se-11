package ru.habibrahmanov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.enumeration.Status;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Project implements Comparable<Project>, Serializable {
    private String id;
    private String userId;
    private String name;
    private String description;
    private Calendar dateBegin;
    private Calendar dateEnd;
    private Status status = Status.PLANNED;

    public Project(String id, String userId, String name, String description, Calendar dateBegin, Calendar dateEnd) {
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.description = description;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }

    @Override
    public int compareTo(@NotNull Project o) {
        return this.getStatus().compareTo(o.getStatus());
    }

    @Override
    @NotNull
    public String toString() {
        return "Project \"" + name + "\"" +
                "\nStatus = " + status.displayName() +
                "\nProject Id = " + id +
                "\nDescription = '" + description + '\'' +
                "\nDateBegin = " + dateBegin.getTime() +
                "\nDateEnd = " + dateEnd.getTime() +
                "\nUser Id = " + userId +
                "\n";
    }
}