package ru.habibrahmanov.tm.entity;

import lombok.Getter;
import lombok.Setter;
import ru.habibrahmanov.tm.enumeration.Status;
import java.util.Calendar;

public abstract class AbstractEntity {
    protected String id;
    protected String userId;
    protected String name;
    protected String description;
    protected Calendar dateBegin;
    protected Calendar dateEnd;
    protected Status status = Status.PLANNED;
}
