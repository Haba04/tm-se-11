package ru.habibrahmanov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.enumeration.Role;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public final class User implements Serializable {
    private String id = UUID.randomUUID().toString();
    private String login;
    private String password;
    private Role role;

    public User(String login, String password, Role role) {
        this.login = login;
        this.password = password;
        this.role = role;
    }

    @Override
    @NotNull
    public String toString() {
        return "Login \"" + login + "\"" +
                "\nRole = " + role.displayName() +
                "\nId = " + id +
                "\n";
    }
}
