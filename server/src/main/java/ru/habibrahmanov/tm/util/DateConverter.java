package ru.habibrahmanov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public final class DateConverter {
    public static Calendar dateConverter(@Nullable String dateString) throws IncorrectValueException {
        if (dateString == null || dateString.isEmpty()) throw new IncorrectValueException();
        @NotNull final DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        @NotNull Date date;
        @NotNull final Calendar calendarDate = Calendar.getInstance();
        try {
            date = dateFormat.parse(dateString);
            calendarDate.setTime(date);
            calendarDate.toString();
            return calendarDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendarDate;
    }

}
