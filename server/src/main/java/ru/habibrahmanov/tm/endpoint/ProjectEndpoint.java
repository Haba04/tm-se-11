package ru.habibrahmanov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.IProjectEndpoint;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.entity.Session;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.exeption.RemoveFailedException;
import ru.habibrahmanov.tm.exeption.SessionIsNotValidException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.text.ParseException;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.habibrahmanov.tm.api.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @Override
    @WebMethod
    public void insertProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description,
            @WebParam(name = "dateBegin", partName = "dateBegin") @Nullable final String dateBegin,
            @WebParam(name = "dateEnd", partName = "dateEnd") @Nullable final String dateEnd
    ) throws ParseException, IncorrectValueException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        endpointLocator.getProjectService().insert(name, description, dateBegin, dateEnd, session.getUserId());
    }

    @Override
    @WebMethod
    public void persistProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "project", partName = "project") @Nullable final Project project
    ) throws IncorrectValueException, ParseException, SessionIsNotValidException {
        if (project == null) throw new IncorrectValueException();
        endpointLocator.getSessionService().validate(session);
        endpointLocator.getProjectService().persist(project);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> findAllProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws SessionIsNotValidException, ListIsEmptyExeption, IncorrectValueException {
        endpointLocator.getSessionService().validate(session);
        return endpointLocator.getProjectService().findAll(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> findAllSortedByDateBeginProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws IncorrectValueException, ListIsEmptyExeption, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        return endpointLocator.getProjectService().findAllSortedByDateBegin(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> findAllSortedByDateEndProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws IncorrectValueException, ListIsEmptyExeption, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        return endpointLocator.getProjectService().findAllSortedByDateEnd(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> findAllSortedByStatusProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws IncorrectValueException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        return endpointLocator.getProjectService().findAllSortedByStatus(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> searchByStringProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "string", partName = "string") @Nullable final String string
    ) throws ListIsEmptyExeption, IncorrectValueException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        return endpointLocator.getProjectService().searchByString(session.getUserId(), string);
    }

    @Override
    @WebMethod
    public void removeAllProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws RemoveFailedException, IncorrectValueException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        endpointLocator.getProjectService().removeAll(session.getUserId());
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws RemoveFailedException, IncorrectValueException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        endpointLocator.getProjectService().remove(projectId, session.getUserId());
    }

    @Override
    @WebMethod
    public void updateProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description,
            @WebParam(name = "dateBegin", partName = "dateBegin") @Nullable final String dateBegin,
            @WebParam(name = "dateEnd", partName = "dateEnd") @Nullable final String dateEnd
    ) throws IncorrectValueException, ParseException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        endpointLocator.getProjectService().update(session.getUserId(), projectId, name, description, dateBegin, dateEnd);
    }

    @Override
    @WebMethod
    public void mergeProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description,
            @WebParam(name = "dateBegin", partName = "dateBegin") @Nullable final String dateBegin,
            @WebParam(name = "dateEnd", partName = "dateEnd") @Nullable final String dateEnd
    ) throws IncorrectValueException, ParseException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        endpointLocator.getProjectService().merge(projectId, name, description, dateBegin, dateEnd, session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> findByAddingProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws ListIsEmptyExeption, IncorrectValueException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        return endpointLocator.getProjectService().findAll(session.getUserId());
    }
}
