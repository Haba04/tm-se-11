package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.Session;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.exeption.RoleTypeException;
import ru.habibrahmanov.tm.exeption.SessionIsNotValidException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.text.ParseException;

@WebService
public interface IDomainEndpoint {

    String URL = "http://localhost:8080/DomainEndpoint?wsdl";

    @WebMethod
    void deserializationDomain(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws IOException, SessionIsNotValidException, ClassNotFoundException, ParseException, IncorrectValueException, RoleTypeException;

    @WebMethod
    void serializationDomain(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws IncorrectValueException, ListIsEmptyExeption, SessionIsNotValidException, IOException, RoleTypeException;

    @WebMethod
    void loadJacksonJson(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws IOException, ParseException, IncorrectValueException, SessionIsNotValidException, RoleTypeException;

    @WebMethod
    void loadJaxbJson(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws JAXBException, ParseException, IncorrectValueException, SessionIsNotValidException, RoleTypeException;

    @WebMethod
    void saveJacksonJson(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws SessionIsNotValidException, ListIsEmptyExeption, IOException, IncorrectValueException, RoleTypeException;

    @WebMethod
    void saveJaxbJson(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws JAXBException, IncorrectValueException, ListIsEmptyExeption, SessionIsNotValidException, RoleTypeException;

    @WebMethod
    void loadJacksonXml(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws IOException, ParseException, IncorrectValueException, SessionIsNotValidException, RoleTypeException;

    @WebMethod
    void loadJaxbXml(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws ParseException, IncorrectValueException, SessionIsNotValidException, JAXBException, RoleTypeException;

    @WebMethod
    void saveJacksonXml(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws SessionIsNotValidException, IOException, IncorrectValueException, ListIsEmptyExeption, JAXBException, RoleTypeException;

    @WebMethod
    void SaveJaxbXml(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws JAXBException, IncorrectValueException, ListIsEmptyExeption, SessionIsNotValidException, RoleTypeException;
}
