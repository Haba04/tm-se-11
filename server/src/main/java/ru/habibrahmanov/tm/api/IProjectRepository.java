package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.Project;
import java.util.Calendar;
import java.util.List;

public interface IProjectRepository {
    void persist(@NotNull Project project);

    @NotNull List<Project> findAll(@NotNull String userId);

    @NotNull List<Project> findAll();

    @NotNull List<Project> findAllSortedByDateBegin(@NotNull String userId);

    @NotNull List<Project> findAllSortedByDateEnd(@NotNull String userId);

    @NotNull List<Project> findAllSortedByStatus(@NotNull String userId);

    @NotNull List<Project> searchByString (@NotNull String projectId, @NotNull String string);

    @Nullable Project findOne(@NotNull String projectId, @NotNull String userId);

    boolean removeAll(@NotNull String userId);

    boolean remove(@NotNull String projectId, @NotNull String userId);

    void merge(@NotNull Project project);

    void update(
            @NotNull String userId, @NotNull String projectId, @NotNull String name,
            @NotNull String description, @NotNull Calendar dateBegin, @NotNull Calendar dateEnd
    );
}
