package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.Session;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.SessionIsNotValidException;


public interface ISessionService {
    void validate(@Nullable Session session) throws SessionIsNotValidException;

    @Nullable Session open(@Nullable String login, @Nullable String password) throws IncorrectValueException;

    void close(@Nullable Session session) throws SessionIsNotValidException;
}
