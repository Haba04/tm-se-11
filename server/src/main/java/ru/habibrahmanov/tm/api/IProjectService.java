package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.exeption.RemoveFailedException;

import java.text.ParseException;
import java.util.List;

public interface IProjectService {
    void insert(
            @Nullable String name, @Nullable String description, @Nullable String dateBegin,
            @Nullable String dateEnd, @Nullable String userId
    ) throws IncorrectValueException, ParseException;

    void persist(@Nullable Project project) throws IncorrectValueException, ParseException;

    @NotNull List<Project> findAll(@NotNull String userId) throws IncorrectValueException, ListIsEmptyExeption;

    @NotNull List<Project> findAll() throws ListIsEmptyExeption, IncorrectValueException;

    @NotNull List<Project> findAllSortedByDateBegin(@NotNull String userId) throws ListIsEmptyExeption, IncorrectValueException;

    @NotNull List<Project> findAllSortedByDateEnd(@NotNull String userId) throws ListIsEmptyExeption, IncorrectValueException;

    @NotNull List<Project> findAllSortedByStatus(@Nullable String userId) throws IncorrectValueException;

    @NotNull List<Project> searchByString (@Nullable String projectId, @Nullable String string) throws IncorrectValueException, ListIsEmptyExeption;

    void removeAll(@Nullable String userId) throws IncorrectValueException, RemoveFailedException;

    void remove(@Nullable String projectId, @Nullable String userId) throws IncorrectValueException, RemoveFailedException;

    void update(@NotNull String userId, @Nullable String projectId, @Nullable String name, @Nullable String description,
                @Nullable String dateBegin, @Nullable String dateEnd) throws IncorrectValueException, ParseException;

    void merge(@Nullable String projectId, @Nullable String name, @Nullable String description,
               @Nullable String dateBegin, @Nullable String dateEnd, @Nullable String userId) throws IncorrectValueException, ParseException;

    @NotNull List<Project> findByAdding(@NotNull String userId);
}
