package ru.habibrahmanov.tm.api;

import ru.habibrahmanov.tm.entity.Session;

public interface ISessionRepository {

    void persist(Session session);

    void remove(Session session);
}
