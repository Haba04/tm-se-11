package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.User;

import java.util.List;

public interface IUserRepository {
    void persist(@NotNull User user);

    @Nullable User findOne(@NotNull String userId);

    @Nullable User findByLogin(@NotNull String login);

    @NotNull List<User> findAll();

    void removeOne(@NotNull String login);

    void removeAll();

    void update(@NotNull String userId, @NotNull String login);

    void merge(@NotNull User user);
}
