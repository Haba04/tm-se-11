package ru.habibrahmanov.tm.comporator;

import ru.habibrahmanov.tm.entity.Task;
import java.util.Comparator;

public final class TaskDateEndComparator implements Comparator<Task> {
    @Override
    public int compare(Task o1, Task o2) {
        return o1.getDateEnd().compareTo(o2.getDateEnd());
    }
}