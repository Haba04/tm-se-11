package ru.habibrahmanov.tm;

import ru.habibrahmanov.tm.bootstrap.Bootstrap;

public class Application {
    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}