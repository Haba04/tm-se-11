package ru.habibrahmanov.tm.repository;

import ru.habibrahmanov.tm.api.ISessionRepository;
import ru.habibrahmanov.tm.entity.Session;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public void persist(Session session) {
        entities.put(session.getId(), session);
    }

    @Override
    public void remove(Session session) {
        entities.remove(session.getId());
    }
}
