package ru.habibrahmanov.tm.repository;

import java.util.LinkedHashMap;
import java.util.Map;

public abstract class AbstractRepository<T> {
    protected final Map<String, T> entities = new LinkedHashMap<>();
    public abstract void persist(final T t);
}
