package ru.habibrahmanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.IUserRepository;
import ru.habibrahmanov.tm.api.IUserService;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.enumeration.Role;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.util.HashUtil;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public final class UserService extends AbstractService implements IUserService {
    private final IUserRepository userRepository;
    @Nullable private User currentUser;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void registryAdmin(
            @Nullable final String login, @Nullable final String password, @Nullable final String passwordConfirm
    ) throws IllegalArgumentException {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (userRepository.findByLogin(login) != null) throw new IllegalArgumentException("SUCH USER EXISTS");
        if (!password.equals(passwordConfirm)) throw new IllegalArgumentException("PASSWORDS DO NOT MATCH");
        userRepository.persist(new User(login, HashUtil.md5(password), Role.ADMIN));
    }

    @Override
    public void registryUser(@Nullable final User user) throws IllegalArgumentException {
        if (user == null) throw new IllegalArgumentException();
        user.setPassword(HashUtil.md5(user.getPassword()));
        userRepository.persist(user);
    }

    @Override
    public void updatePassword(
            @Nullable final User currentUser, @Nullable final String curPassword, @Nullable final String newPassword,
            @Nullable final String newPasswordConfirm) {
        if (currentUser == null) return;
        if (curPassword == null || curPassword.isEmpty()) return;
        if (newPassword == null || newPassword.isEmpty()) return;
        if (newPasswordConfirm == null || newPasswordConfirm.isEmpty()) return;
        if (!currentUser.getPassword().equals(HashUtil.md5(curPassword))) {
            throw new IllegalArgumentException("CURRENT PASSWORD DOES NOT MATCH USER PASSWORD: " + currentUser.getLogin());
        }
        if (!newPassword.equals(newPasswordConfirm)) throw new IllegalArgumentException("PASSWORDS DO NOT MATCH");
        currentUser.setPassword(HashUtil.md5(newPassword));
    }

    @Nullable
    @Override
    public User login(@Nullable final String login, @Nullable final String password) throws IncorrectValueException {
        if (login == null || login.isEmpty()) throw new IncorrectValueException();
        if (password == null || password.isEmpty()) throw new IncorrectValueException();
        @Nullable final List<User> userList = userRepository.findAll();
        if (userList.isEmpty()) throw new IncorrectValueException("NO USER CREATED");
        if (userRepository.findByLogin(login) == null) throw new IncorrectValueException("USER WITH SUCH LOGIN DOES NOT EXIST");
        final String currentPassword = userRepository.findByLogin(login).getPassword();
        final String passwordMD5 = HashUtil.md5(password);
        if (!currentPassword.equals(passwordMD5)) throw new IncorrectValueException("WRONG PASSWORD");
        currentUser = userRepository.findByLogin(login);
        return currentUser;
    }

    @Nullable
    @Override
    public User viewProfile() {
        return currentUser;
    }

    @NotNull
    @Override
    public List<User> findAll() throws ListIsEmptyExeption {
        if (userRepository.findAll().isEmpty()) throw new ListIsEmptyExeption();
        return userRepository.findAll();
    }

    @NotNull
    @Override
    public User findOne(@Nullable final String userId) throws IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        return userRepository.findOne(userId);
    }

    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) throws ListIsEmptyExeption {
        if (login == null || login.isEmpty()) throw new ListIsEmptyExeption();
        return userRepository.findByLogin(login);
    }

    @Override
    public void editProfile(@Nullable final String newLogin) throws IncorrectValueException {
        if (newLogin == null || newLogin.isEmpty()) throw new IncorrectValueException();
        if (currentUser == null) throw new IncorrectValueException();
        currentUser.setLogin(newLogin);
    }

    @Override
    public void logout() {
        currentUser = null;
    }

    @Override
    public boolean isAuth() {
        return currentUser != null;
    }

    @Override
    public User getCurrentUser() {
        return currentUser;
    }
}
